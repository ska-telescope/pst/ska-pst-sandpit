SKA-PST-COMMON
==============

This project provides common code and libraries used by the Square
Kilometre Array (SKA)'s Pulsar Timing (PST) instrument. It also
includes gRPC and Protobuf definitions that are used for integration
between the ska-pst-lmc project and the signal processing software
components.

.. README =============================================================

.. toctree::
  :maxdepth: 1
  :caption: Readme
  :hidden:

   ../../README.md

.. COMMUNITY SECTION ==================================================

..

.. toctree::
  :maxdepth: 3
  :caption: API
  :hidden:

  Common API<api/library_root>
  gRPC + Protobuf<api/protobuf>
