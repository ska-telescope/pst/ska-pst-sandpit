variables:
  # Needed if you want automatic submodule checkout
  # For details see https://docs.gitlab.com/ee/ci/yaml/README.html#git-submodule-strategy
  GIT_SUBMODULE_STRATEGY: recursive
  SKA_CPP_DOCKER_BUILDER_IMAGE: artefact.skao.int/ska-cicd-cpp-build-base:0.2.10
  K8S_TEST_CLUSTER_TAG: k8srunner
  SKA_RELEASE_REGISTRY: artefact.skao.int
  GITLAB_OCI_COMMON_REGISTRY: registry.gitlab.com/ska-telescope/pst/ska-pst-common
  GITLAB_OCI_BUILDTOOLS_REGISTRY: registry.gitlab.com/ska-telescope/pst/ska-pst-buildtools
  # OCI variables that overrides values in Makefile
  # CI Variables enable upstream/downstream CI integration
  PST_OCI_BUILDTOOLS_REGISTRY: $SKA_RELEASE_REGISTRY
  PST_OCI_BUILDTOOLS_IMAGE: ska-pst-buildtools
  PST_OCI_BUILDTOOLS_TAG: "0.0.2"
  PST_OCI_COMMON_REGISTRY: $SKA_RELEASE_REGISTRY
  PST_OCI_COMMON_BUILDER: ska-pst-common-builder
  PST_OCI_COMMON_PROTO: ska-pst-common-proto
  PST_OCI_COMMON_TAG: ""
  OCI_IMAGE_BUILD_CONTEXT: $(PWD)
  OCI_IMAGE: "$PST_OCI_COMMON_BUILDER $PST_OCI_COMMON_PROTO"

image: $SKA_CPP_DOCKER_BUILDER_IMAGE

.src_dir_ref_storage: &src_dir_ref_storage
  - mkdir build || true
  - pwd > build/original_source_directory

.src_dir_ref_replacement: &src_dir_ref_replacement
  - old_sdir=`cat build/original_source_directory`
  - this_sdir=`pwd`
  - find build
    -type f -a
    \! \(
      -perm /111 -o
      \(
        -name '*.o' -o -name '*.a' -o -name '*.so'
      \)
    \)
    -exec sed -i "s|$old_sdir|$this_sdir|g" {} +

stages:
  - build
  - lint
  - test
  - publish
  - scan
  - pages

oci-image-build:
  script:
    - make oci-build-all CAR_OCI_REGISTRY_HOST=${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME} OCI_BUILD_ADDITIONAL_ARGS=" --build-arg BUILD_IMAGE=$PST_OCI_BUILDTOOLS_REGISTRY/$PST_OCI_BUILDTOOLS_IMAGE:$PST_OCI_BUILDTOOLS_TAG"
  rules:
    - if: ($CI_COMMIT_BRANCH =~ /at3.*|skb.*|main/) || (($CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /at3.*|skb.*/) && ($CI_PIPELINE_SOURCE == "merge_request_event"))
      variables:
        PST_OCI_COMMON_REGISTRY: ${GITLAB_OCI_COMMON_REGISTRY}
        PST_OCI_BUILDTOOLS_REGISTRY: ${GITLAB_OCI_BUILDTOOLS_REGISTRY}
    - if: ($CI_COMMIT_TAG) || ($CI_COMMIT_BRANCH =~ /rel.*|main/) || (($CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /rel.*/) && ($CI_PIPELINE_SOURCE == "merge_request_event"))
      variables:
        PST_OCI_COMMON_REGISTRY: ${SKA_RELEASE_REGISTRY}
        PST_OCI_BUILDTOOLS_REGISTRY: ${SKA_RELEASE_REGISTRY}

oci-image-publish:
  when: manual

include:
  # OCI
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/oci-image.gitlab-ci.yml'

  # Docs pages
  # - project: 'ska-telescope/templates-repository'
  #   file: 'gitlab-ci/includes/docs.gitlab-ci.yml'

  # Create Gitlab CI badges from CI metrics
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/finaliser.gitlab-ci.yml'

  # Umbrella include for all Raw life cycle stages
  - project: 'ska-telescope/templates-repository'
    file: gitlab-ci/includes/release.gitlab-ci.yml

  # Cpp lifecycle
  - local: '.gitlab/ci/cpp.gitlab-ci.yml'